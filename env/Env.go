package env

import "os"

type EnvConfig struct {
	KafkaUrl   string
	KafkaTopic string
}

func LoadEnvConfig() EnvConfig {
	kafkaHost := os.Getenv("KAFKA_HOST")
	kafkaPort := os.Getenv("KAFKA_PORT")
	kafkaTopic := os.Getenv("KAFKA_TOPIC")

	if len(kafkaHost) == 0 {
		kafkaHost = "localhost"
	}
	if len(kafkaPort) == 0 {
		kafkaPort = "9092"
	}
	if len(kafkaTopic) == 0 {
		kafkaTopic = "grestpio-topic"
	}

	return EnvConfig{
		KafkaUrl:   kafkaHost + ":" + kafkaPort,
		KafkaTopic: kafkaTopic,
	}
}
