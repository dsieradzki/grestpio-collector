package model

import (
	"encoding/json"
	"log"
)

type ChangeStateEvent struct {
	Pin   string
	State string
}

func ToJSON(event ChangeStateEvent) []byte {
	bytes, err := json.Marshal(event)
	if err != nil {
		log.Fatal(err)
	}
	return bytes
}
