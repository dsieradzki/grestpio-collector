package main

import (
	"context"
	"gRESTpIO-Collector/config"
	"gRESTpIO-Collector/env"
	"gRESTpIO-Collector/model"
	"github.com/segmentio/kafka-go"
	"log"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"syscall"
)

var messagesChannel = make(chan model.ChangeStateEvent, 1000) //Buffered channel with 1000 elements
var signalChannel = make(chan os.Signal)
var configService = new(config.ConfigService)

func handler(w http.ResponseWriter, request *http.Request) {
	log.Print("REST - data received")
	loadedConfig := configService.LoadConfig()
	event := parseRequest(&loadedConfig, request)
	if event != nil {
		messagesChannel <- *event
	}
}

func parseRequest(config *config.Config, r *http.Request) *model.ChangeStateEvent {
	for _, conf := range config.Rules {
		urlPattern := regexp.MustCompile(conf.UrlPattern)
		urlMatch := urlPattern.FindStringSubmatch(r.URL.Path)
		if len(urlMatch) > 1 {
			return &model.ChangeStateEvent{
				Pin:   urlMatch[1],
				State: urlMatch[2],
			}
		}
	}
	return nil
}

func main() {

	server := http.Server{Addr: ":8080"}

	signal.Notify(signalChannel, syscall.SIGTERM)
	signal.Notify(signalChannel, syscall.SIGINT)

	go func() {
		http.HandleFunc("/", handler)
		log.Fatal(server.ListenAndServe())
	}()

	envConfig := env.LoadEnvConfig()
	conn, err := kafka.DialLeader(context.Background(), "tcp", envConfig.KafkaUrl, envConfig.KafkaTopic, 1)

	if err != nil {
		log.Fatal("Cannot connect to kafka server [%s] with topic [%s] ", envConfig.KafkaUrl, envConfig.KafkaTopic)
	}

	go func() {
		_ = <-signalChannel
		_ = conn.Close()
		_ = server.Shutdown(context.Background())
		log.Print("[SIGNAL] - received request to close application")
	}()

	log.Print("gRESTpIO Collector Ready for data")
	for {
		select {
		case data := <-messagesChannel:
			{
				_, err := conn.WriteMessages(kafka.Message{Value: model.ToJSON(data)})
				if err != nil {
					log.Fatal(err)
				}
			}
		}
	}

}
