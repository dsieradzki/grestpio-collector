package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type Config struct {
	Rules []ConfigRule `json:"rules"`
}

type ConfigRule struct {
	UrlPattern  string `json:"url-pattern"`
	BodyPattern string `json:"body-pattern"`
}

type ConfigService struct {
}

func (self *ConfigService) LoadConfig() Config {
	data, err := ioutil.ReadFile("./config.json")
	config := Config{}

	if err != nil {
		log.Fatal(err)
		return config
	}

	err = json.Unmarshal(data, &config)
	if err != nil {
		log.Fatal(err)
	}
	return config
}
